import sys
from scli.shiny_cli import ShinyCLI


def main(args=None):
    if not args:
        args = sys.argv[1:]

    shiny_cli = ShinyCLI()
    shiny_cli.run(args)
